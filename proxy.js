const fetch = require('node-fetch');

module.exports = async (req, res) => {
  // Define the target URL you want to proxy to
  const targetUrl = 'https://google.com' + req.url;

  try {
    const response = await fetch(targetUrl, {
      method: req.method,
      headers: req.headers,
      body: req.body
    });

    const data = await response.text();

    // Forward the response from the target server
    res.status(response.status).send(data);
  } catch (error) {
    console.error('Proxy error:', error);
    res.status(500).send('Proxy error');
  }
};

